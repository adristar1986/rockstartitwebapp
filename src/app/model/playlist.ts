import {Songs} from "./songs";

export class Playlist {
  id: number;
  name: string;
  songs: [Songs];
}
