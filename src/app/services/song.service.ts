import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Songs} from '../model/songs';
import {Playlist} from "../model/playlist";

@Injectable({
  providedIn: 'root'
})
export class SongService {
  private songsUrl = 'http://localhost:3000/songs';  // URL to web api

  constructor(private http: HttpClient) {
    this.loadFirstPlaylist();
  }

  getArtistSongs(name: string): Observable<Songs> {
    const url = `${this.songsUrl}/?artist=${name}`;
    return this.http.get<Songs>(url).pipe(
      catchError(this.handleError<Songs>(`getSong id=${name}`))
    );
  }

  getPlaylists() {
    const playlist: Playlist[] = JSON.parse(localStorage.getItem('playlist'));
    return playlist;
  }

  createNewPlaylist(newPlaylistName: any) {
    const playlist: Playlist[] = JSON.parse(localStorage.getItem('playlist'));
    playlist.push(newPlaylistName);
    localStorage.setItem('playlist', JSON.stringify(playlist));
  }

  /**
   * GenId method ensure that a playlist always has an id by creating from new playlist.
   * The method returns the initial number (0).
   * if the playlist array is not empty, the method below returns the highest playlist id + 1.
   *
   * @param newPlaylistName is the name from the new playlist.
   */
  genId(newPlaylistName): number {
    return newPlaylistName.length > 0 ? Math.max(...newPlaylistName.map(playlist => playlist.id)) + 1 : 0;
  }

  deletePlaylist(playlistId: number) {
    const playlist = JSON.parse(localStorage.getItem('playlist'));

    for (let i = 0; i < playlist.length; i++) {
      if (playlist[i].id === playlistId) {
        playlist.splice(i, 1);
      }
    }
    localStorage.setItem('playlist', JSON.stringify(playlist));
  }

  updatePlaylist(playlistId: number, oldName: string, newName: string) {
    const playlist = JSON.parse(localStorage.getItem('playlist'));

    for (let i = 0; i < playlist.length; i++) {
      if (playlist[i].name === oldName && playlist[i].id === playlistId) {
        playlist[i].name = newName;
      }
    }
    localStorage.setItem('playlist', JSON.stringify(playlist));
  }

  getPlaylistSongs(playlistId: number, playlistName: string) {
    const playlist = JSON.parse(localStorage.getItem('playlist'));
    const playlistSongs = [];

    for (let i = 0; i < playlist.length; i++) {
      if (playlist[i].name === playlistName && playlist[i].id === playlistId) {
        for (let l = 0; l < playlist[i].songs.length; l++) {
          playlistSongs.push(playlist[i].songs[l]);
        }
        return playlistSongs;
      }
    }
  }

  addSong(playlistId: number, playlistName: string, song: any) {
    const playlist = JSON.parse(localStorage.getItem('playlist'));

    for (let i = 0; i < playlist.length; i++) {
      if (playlist[i].name === playlistName && playlist[i].id === playlistId) {
        playlist[i].songs.push(song);
      }
    }
    localStorage.setItem('playlist', JSON.stringify(playlist));
  }

  deleteSong(playlistId: number, songId: number) {
    const playlist = JSON.parse(localStorage.getItem('playlist'));
    console.log(playlistId);
    for (let i = 0; i < playlist.length; i++) {
      if (playlist[i].id === playlistId) {
        for (let l = 0; l < playlist[i].songs.length; l++) {
          if (playlist[i].songs[l].id === songId) {
            playlist[i].songs.splice(l, 1);
          }
        }
      }
    }
    localStorage.setItem('playlist', JSON.stringify(playlist));
  }

  /**
   * Create a new playlist with songs
   */
  loadFirstPlaylist() {
    if (localStorage.getItem('playlist') === null || localStorage.getItem('playlist') === undefined) {
      console.log('No playlist Found... Creating...');
      const playlist = [
        {
          id: 1,
          name: 'Post Malone',
          songs: [{id: 1, songName: 'wow'}]
        }
      ];

      localStorage.setItem('playlist', JSON.stringify(playlist));
      return;
    } else {
      console.log('Found playlist...');
    }
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
