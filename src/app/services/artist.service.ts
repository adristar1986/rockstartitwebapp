import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Artists} from '../model/artists';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {
  private artistsUrl = 'http://localhost:3000/artists';

  constructor(private http: HttpClient) {
  }

  getArtists(): Observable<Artists[]> {
    return this.http.get<Artists[]>(this.artistsUrl).pipe(
      catchError(this.handleError<Artists[]>('getArtists', []))
    );
  }

  getArtistById(id: number): Observable<Artists> {
    const url = `${this.artistsUrl}/${id}`;
    return this.http.get<Artists>(url).pipe(
      catchError(this.handleError<Artists>(`getArtist id=${id}`))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      return of(result as T);
    };
  }
}
