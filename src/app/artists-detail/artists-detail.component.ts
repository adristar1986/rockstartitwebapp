import {Component, OnInit, Input} from '@angular/core';
import {Artists} from '../model/artists';
import {ActivatedRoute} from '@angular/router';
import {ArtistService} from '../services/artist.service';
import {Location} from '@angular/common';
import {SongService} from '../services/song.service';
import {Songs} from '../model/songs';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-artists-detail',
  templateUrl: './artists-detail.component.html',
  styleUrls: ['./artists-detail.component.css']
})
export class ArtistsDetailComponent implements OnInit {
  @Input() artist: Artists;
  @Input() song: Songs;
  playlists = [];

  constructor(private route: ActivatedRoute, private artistService: ArtistService,
              private location: Location, private  songService: SongService, private toastr: ToastrService) {
  }

  ngOnInit() {
    this.getArtistById();
    this.playlists = this.songService.getPlaylists();
  }

  getArtistById() {
    const id = +this.route.snapshot.paramMap.get('id');
    return this.artistService.getArtistById(id)
      .subscribe((data => {
          this.artist = data;
          const name = this.artist.name;
          this.getArtistSongs(name);
        }),
      );
  }

  getArtistSongs(name: string): void {
    this.songService.getArtistSongs(name)
      .subscribe(songs => this.song = songs);
  }

  addSongToPlaylist(playlistId: number, playlistName: string, songId: number, songName: string) {
    const addSongToPlaylist = {
      id: songId,
      songName: songName
    };
    if (playlistName) {
      this.songService.addSong(playlistId, playlistName, addSongToPlaylist);
      this.showSuccessMsg(songName + ' was add to playlist ' + playlistName);
    }
  }

  showSuccessMsg(msg: string) {
    this.toastr.success(msg);
  }

  goBack(): void {
    this.location.back();
  }
}
