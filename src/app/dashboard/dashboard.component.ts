import {Component, OnInit, ViewChild, HostListener} from '@angular/core';
import {Artists} from '../model/artists';
import {ArtistService} from '../services/artist.service';
import {Location} from '@angular/common';
import {MdbTableDirective} from 'angular-bootstrap-md';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @ViewChild(MdbTableDirective) mdbTable: MdbTableDirective;
  artists: Artists[] = [];
  searchText = '';
  previous: string;

  constructor(private location: Location, private artistService: ArtistService) {

  }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  ngOnInit() {
    this.getArtists();
  }

  getArtists() {
    return this.artistService.getArtists()
      .subscribe((data => {

          this.artists = data;

          this.mdbTable.setDataSource(this.artists);
          this.artists = this.mdbTable.getDataSource();
          this.previous = this.mdbTable.getDataSource();
        }),
      );
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.artists = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.artists = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }
  }

}
