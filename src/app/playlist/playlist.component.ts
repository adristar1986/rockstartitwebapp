import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from 'angular-bootstrap-md';
import {SongService} from '../services/song.service';
import {Playlist} from "../model/playlist";

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {
  @ViewChild(ModalDirective) modal: ModalDirective;
  @ViewChild('basicModal1') view: any;
  @ViewChild('newName') newName: any;
  playlists = [];
  playlistSongs = [];
  playlistId: number;
  oldPlaylistName: string;
  playlistName: string;
  editStatus = 'default';
  newNameInputField = '';


  constructor(private songService: SongService) {

  }

  ngOnInit() {
    this.playlists = this.songService.getPlaylists();
    console.log(this.playlists);
  }

  addNewPlaylist(newPlaylistName: string) {
    const playlist: Playlist[] = JSON.parse(localStorage.getItem('playlist'));
    const newPlaylistId = this.songService.genId(playlist);

    const newPlaylist = {
      id: newPlaylistId,
      name: newPlaylistName,
      songs: []
    };
    if (newPlaylistName) {
      this.playlists.push(newPlaylist);

      this.songService.createNewPlaylist(newPlaylist);
      this.newNameInputField = null;
      this.modal.hide();
      this.newName.nativeElement.value = null;
    }
  }

  deletePlaylist(playlistId: number) {
    console.log(playlistId);
    for (let i = 0; i < this.playlists.length; i++) {
      if (this.playlists[i].id === playlistId) {
        this.playlists.splice(i, 1);
      }
    }
    this.songService.deletePlaylist(playlistId);
  }

  editPlaylist(playlistName: string, playlistId: number) {
    this.editStatus = 'edit';
    this.oldPlaylistName = playlistName;
    this.playlistId = playlistId;
    this.modal.show();
    this.newNameInputField = playlistName;
  }

  updatePlaylistName(playlistId: number, newName: string) {
    for (let i = 0; i < this.playlists.length; i++) {
      if (this.playlists[i].id === this.playlistId) {
        this.playlists[i].name = newName;
      }
    }
    this.songService.updatePlaylist(playlistId, this.oldPlaylistName, newName);
    this.modal.hide();
    this.editStatus = 'default';
    this.newNameInputField = null;
  }

  viewPlaylistDetails(playlistId: number, playlistName: string) {
    this.playlistName = playlistName;
    this.playlistId = playlistId;
    this.songService.getPlaylistSongs(playlistId, playlistName);
    this.playlistSongs = this.songService.getPlaylistSongs(playlistId, playlistName);
    this.view.show();
  }

  deletePlaylistSong(songId: number) {

    for (let i = 0; i < this.playlistSongs.length; i++) {
      if (this.playlistSongs[i].id === songId) {
        this.playlistSongs.splice(i, 1);
      }

      this.songService.deleteSong(this.playlistId, songId);
    }
  }

  cancelBtn() {
    this.modal.hide();
    this.editStatus = 'default';
    this.newNameInputField = null;
    this.newName.nativeElement.value = null;
  }
}
